#!/usr/bin/perl

# ------------------------------------------------------------------------------------
# filename:	kThumb.pl
# author:	Frode Klevstul (frode@klevstul.com) (www.klevstul.com)
# started:	16.02.2013
# ------------------------------------------------------------------------------------
#
# Revision - Newest version at the top - - - - - - - - - - - - - - - - - - - - - - - -
#
# version:  v01_20130216
#           - The first version of this program was made.
# ------------------------------------------------------------------------------------


# -------------------
# use
# -------------------
use strict;
use warnings;
use LWP::Simple;

# -------------------
# declarations
# -------------------
my $url				= "http://img.chronicles.no/feeds/posts/default?max-results=20";
my $outputFile		= "/home/klevstul/www/klevstulCom/scripts/kThumb/gadget.html";
my $noPicsLarge		= 5;
my $noPicsSmall		= 10;

# -------------------
# main
# -------------------
my $object = get($url);

# remove everything before first <entry> tag
$object =~ s/.*?(<entry.*)/$1/sgi;

# split and save entries into @entry
my @entry = split /<entry/, $object;

# remove first entry of the array, as that is blank
shift @entry;

# declare some more variables that is needed
my $entry_;
my @link;
my $link_;
my @img;
my $img_;
my @title;
my $title_;

foreach $entry_ (@entry){

	$link_	= $entry_;
	$img_	= $entry_;
	$title_	= $entry_;

	#print "\n----------------------- [ENTRY] ----------------------\n";

	# get the link to the blog post
	if ($link_ =~ m/<feedburner:origLink>(.*)<\/feedburner:origLink>/sgi) {
		$link_ = $1;
	} else {
		$link_ = "";
	}

	# get the URL to the image
	# <img border="0" height="640" src="http://2.bp.blogspot.com/-JFn_8CKM4IQ/UR9jQ6FYTYI/AAAAAAAAMwY/GPzS9vKSt1M/s640/page.jpg" width="640" />
	if ($img_ =~ m/img.*?src=["'](.*?)["']/sgi) {
		$img_ = $1;
	} else {
		$img_ = "";
	}

	# get the title of the blog post
	if ($title_ =~ m/<title type="text">(.*)<\/title>/sgi) {
		$title_ = $1;
		# html special characters : http://ascii.cl/htmlcodes.htm
		$title_ =~ s/�/&aelig;/sg;
		$title_ =~ s/�/&AElig;/sg;
		$title_ =~ s/�/&oslash;/sg;
		$title_ =~ s/�/&Oslash;/sg;
		$title_ =~ s/�/&aring;/sg;
		$title_ =~ s/�/&Aring;/sg;
		$title_ =~ s/�/&Agrave;/sg;
		$title_ =~ s/�/&Aacute;/sg;
		$title_ =~ s/�/&Acirc;/sg;
		$title_ =~ s/�/&Atilde;/sg;
		$title_ =~ s/�/&Auml;/sg;
		$title_ =~ s/�/&Egrave;/sg;
		$title_ =~ s/�/&Eacute;/sg;
		$title_ =~ s/�/&Ecirc;/sg;
		$title_ =~ s/�/&Euml;/sg;
		$title_ =~ s/�/&Uuml;/sg;
		$title_ =~ s/�/&agrave;/sg;
		$title_ =~ s/�/&aacute;/sg;
		$title_ =~ s/�/&acirc;/sg;
		$title_ =~ s/�/&atilde;/sg;
		$title_ =~ s/�/&auml;/sg;
		$title_ =~ s/�/&egrave;/sg;
		$title_ =~ s/�/&eacute;/sg;
		$title_ =~ s/�/&ecirc;/sg;
		$title_ =~ s/�/&euml;/sg;
		$title_ =~ s/�/&uuml;/sg;
		$title_ =~ s/"/'/sg;
	} else {
		$title_ = "";
	}

	# store the collected info in some arrays
	push @link, $link_;
	push @img, $img_;
	push @title, $title_;

	#print "LINK :: $link_ \n";
	#print "IMG :: $img_ \n";
}

# print html to file
open(FILE, ">$outputFile") || die("failed to open '$outputFile'");
flock (FILE, 2);
print FILE qq(
<?xml version="1.0" encoding="UTF-8"?>
<Module>
<ModulePrefs height="310" title="kThumb">
</ModulePrefs>
	<Content type="html">
		<![CDATA[
			<table style="border-spacing: 0px; border-collapse:collapse;"><tr>
);

my $i = 0;
OUTPUT: foreach (@link){
	if ($i == $noPicsLarge){
		last OUTPUT;
	}
	print FILE qq(<td><a href="$link[$i]" target="_blank">\n);
	print FILE qq(<img style="height: 190px; width: 208px; border: 1px solid black;" src="$img[$i]" alt="$title[$i]" title="$title[$i]"></a>\n);
	print FILE qq(</a></td>\n);
	$i++;
}

print FILE qq(
			</tr>
			
			<table style="border-spacing: 0px; border-collapse:collapse;"><tr>
);

OUTPUT: foreach (@link){
	if ($i == $noPicsLarge + $noPicsSmall){
		last OUTPUT;
	}
	print FILE qq(<td><a href="$link[$i]" target="_blank">\n);
	print FILE qq(<img style="height: 102px; width: 102px; border: 1px solid black;" src="$img[$i]" alt="$title[$i]" title="$title[$i]"></a>\n);
	print FILE qq(</a></td>\n);
	$i++;
}

print FILE qq(
			</tr></table>
		]]>
	</Content>
</Module>
);

flock (FILE, 8);
close (FILE);
